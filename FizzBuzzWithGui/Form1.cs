﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Text.RegularExpressions; 

namespace FizzBuzzWithGui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

           

        }

        private void btnEnter_Click(object sender, EventArgs e)
        {


            int i = 0;
            string output = "";
            string Display = "";
            int cap = 100;
            int Fizz = 3;
            int Buzz = 5;
          


            //wipes the display
            Display = "";
            tbOutput.Text = Display;

            tbOutput.Clear(); 


            
            


            //Get the user inputs from the form 
            try
            {
                i = System.Convert.ToInt32(tbStart.Text); 
                cap = System.Convert.ToInt32(tbCap.Text);
                Fizz = System.Convert.ToInt32(tbFizz.Text);
                Buzz = System.Convert.ToInt32(tbBuzz.Text);

                if (i > cap)
                {
                    MessageBox.Show("The end number must be the largest");

                    tbStart.Text = "0";

                    //wipes the display
                    Display = "";
                    tbOutput.Text = Display;

                }

            }

            //If that doesnt work print an error message
            catch 
            {

                MessageBox.Show("Please Do not leave any text boxes blank");
                //wipes the display
                Display = "";
                tbOutput.Text = Display;

                


            }


            int DifForPb = cap - i; 
            pbMain.Maximum =  DifForPb;
            pbMain.Step = 1; 


            //The fizzbuzz algorithm  
            while (i < cap)
            {
               
                pbMain.PerformStep(); 

                if (i % Fizz == 0)
                {
                    output = "Fizz";
                }

                if (i % Buzz == 0)
                {
                    output += "Buzz";
                }

                if (output == "")
                {
                    output = i.ToString();
                }

                Console.WriteLine(output);

               

                //concatonating all the outputs 
                Display += output + Environment.NewLine;
                

                //resetting the output so it isnt one long string
                output = "";
                
                //increase i by 1
                i++;

               
            }

            //Supposed to clear the progress bar, but doesnt work 
            // if (i == cap)
            // {
            //   pbMain.Value = 0;
            //}





            //This is the Regex that looks for Fizz, Buzz, and FizzBuzz. Will have to do my own later

            int FizzCount = Regex.Matches(Display, "Fizz").Count;
            string FizzLabelContents = "Fizz Count: " + FizzCount.ToString();

            lblFizzCount.Text = FizzLabelContents;
            //lblFizzCount.Text = FizzCount.ToString();



            int BuzzCount = Regex.Matches(Display, "Buzz").Count;
            string BuzzLabelContents = "Buzz Count: " + BuzzCount.ToString();

            lblBuzzCount.Text = BuzzLabelContents;
            //lblFizzCount.Text = FizzCount.ToString();



            int FizzBuzzCount = Regex.Matches(Display, "FizzBuzz").Count;
            string FizzBuzzLabelContents = "FizzBuzz Count: " + FizzBuzzCount.ToString();

            lblFizzBuzzCount.Text = FizzBuzzLabelContents;
            //lblFizzCount.Text = FizzCount.ToString();


            //Sending out the output to the form 
            tbOutput.Text = Display;

          

        }

        private void tbCap_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lblTitle_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
