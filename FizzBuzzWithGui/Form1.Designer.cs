﻿namespace FizzBuzzWithGui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnter = new System.Windows.Forms.Button();
            this.tbCap = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.lblFizz = new System.Windows.Forms.Label();
            this.lblBuzz = new System.Windows.Forms.Label();
            this.tbFizz = new System.Windows.Forms.TextBox();
            this.tbBuzz = new System.Windows.Forms.TextBox();
            this.lblFizzCount = new System.Windows.Forms.Label();
            this.lblBuzzCount = new System.Windows.Forms.Label();
            this.lblFizzBuzzCount = new System.Windows.Forms.Label();
            this.lblCap = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pbMain = new System.Windows.Forms.ProgressBar();
            this.tbStart = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("Arial Black", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnter.Location = new System.Drawing.Point(212, 495);
            this.btnEnter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(311, 126);
            this.btnEnter.TabIndex = 5;
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // tbCap
            // 
            this.tbCap.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCap.Location = new System.Drawing.Point(337, 192);
            this.tbCap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCap.MaxLength = 5;
            this.tbCap.Name = "tbCap";
            this.tbCap.ShortcutsEnabled = false;
            this.tbCap.Size = new System.Drawing.Size(143, 34);
            this.tbCap.TabIndex = 2;
            this.tbCap.Text = "100";
            this.tbCap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbCap.TextChanged += new System.EventHandler(this.tbCap_TextChanged);
            // 
            // tbOutput
            // 
            this.tbOutput.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tbOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbOutput.Dock = System.Windows.Forms.DockStyle.Left;
            this.tbOutput.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOutput.Location = new System.Drawing.Point(0, 0);
            this.tbOutput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbOutput.MaxLength = 100;
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(159, 646);
            this.tbOutput.TabIndex = 4;
            this.tbOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbOutput.WordWrap = false;
            // 
            // lblFizz
            // 
            this.lblFizz.AutoSize = true;
            this.lblFizz.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFizz.Location = new System.Drawing.Point(232, 258);
            this.lblFizz.Name = "lblFizz";
            this.lblFizz.Size = new System.Drawing.Size(44, 22);
            this.lblFizz.TabIndex = 5;
            this.lblFizz.Text = "Fizz";
            // 
            // lblBuzz
            // 
            this.lblBuzz.AutoSize = true;
            this.lblBuzz.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuzz.Location = new System.Drawing.Point(232, 306);
            this.lblBuzz.Name = "lblBuzz";
            this.lblBuzz.Size = new System.Drawing.Size(51, 22);
            this.lblBuzz.TabIndex = 6;
            this.lblBuzz.Text = "Buzz";
            // 
            // tbFizz
            // 
            this.tbFizz.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFizz.Location = new System.Drawing.Point(401, 258);
            this.tbFizz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFizz.MaxLength = 2;
            this.tbFizz.Name = "tbFizz";
            this.tbFizz.Size = new System.Drawing.Size(31, 27);
            this.tbFizz.TabIndex = 3;
            this.tbFizz.Text = "3";
            this.tbFizz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbBuzz
            // 
            this.tbBuzz.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuzz.Location = new System.Drawing.Point(401, 306);
            this.tbBuzz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbBuzz.MaxLength = 2;
            this.tbBuzz.Name = "tbBuzz";
            this.tbBuzz.Size = new System.Drawing.Size(31, 27);
            this.tbBuzz.TabIndex = 4;
            this.tbBuzz.Text = "5";
            this.tbBuzz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFizzCount
            // 
            this.lblFizzCount.AutoSize = true;
            this.lblFizzCount.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFizzCount.Location = new System.Drawing.Point(204, 363);
            this.lblFizzCount.Name = "lblFizzCount";
            this.lblFizzCount.Size = new System.Drawing.Size(109, 22);
            this.lblFizzCount.TabIndex = 10;
            this.lblFizzCount.Text = "Fizz Count: ";
            // 
            // lblBuzzCount
            // 
            this.lblBuzzCount.AutoSize = true;
            this.lblBuzzCount.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuzzCount.Location = new System.Drawing.Point(204, 414);
            this.lblBuzzCount.Name = "lblBuzzCount";
            this.lblBuzzCount.Size = new System.Drawing.Size(116, 22);
            this.lblBuzzCount.TabIndex = 11;
            this.lblBuzzCount.Text = "Buzz Count: ";
            // 
            // lblFizzBuzzCount
            // 
            this.lblFizzBuzzCount.AutoSize = true;
            this.lblFizzBuzzCount.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFizzBuzzCount.Location = new System.Drawing.Point(204, 458);
            this.lblFizzBuzzCount.Name = "lblFizzBuzzCount";
            this.lblFizzBuzzCount.Size = new System.Drawing.Size(150, 22);
            this.lblFizzBuzzCount.TabIndex = 12;
            this.lblFizzBuzzCount.Text = "FizzBuzz Count: ";
            // 
            // lblCap
            // 
            this.lblCap.AutoSize = true;
            this.lblCap.Font = new System.Drawing.Font("Arial", 10.8F);
            this.lblCap.Location = new System.Drawing.Point(204, 204);
            this.lblCap.Name = "lblCap";
            this.lblCap.Size = new System.Drawing.Size(116, 22);
            this.lblCap.TabIndex = 13;
            this.lblCap.Text = "End Number";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Arial Black", 36F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(165, 23);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(387, 85);
            this.lblTitle.TabIndex = 14;
            this.lblTitle.Text = "FIZZBUZZ!";
            this.lblTitle.Click += new System.EventHandler(this.lblTitle_Click);
            // 
            // pbMain
            // 
            this.pbMain.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pbMain.Location = new System.Drawing.Point(187, 111);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(336, 23);
            this.pbMain.TabIndex = 15;
            // 
            // tbStart
            // 
            this.tbStart.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold);
            this.tbStart.Location = new System.Drawing.Point(337, 150);
            this.tbStart.MaxLength = 5;
            this.tbStart.Name = "tbStart";
            this.tbStart.Size = new System.Drawing.Size(143, 34);
            this.tbStart.TabIndex = 1;
            this.tbStart.Text = "0";
            this.tbStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.8F);
            this.label1.Location = new System.Drawing.Point(204, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 22);
            this.label1.TabIndex = 17;
            this.label1.Text = "Start Number";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(575, 646);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbStart);
            this.Controls.Add(this.pbMain);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblCap);
            this.Controls.Add(this.lblFizzBuzzCount);
            this.Controls.Add(this.lblBuzzCount);
            this.Controls.Add(this.lblFizzCount);
            this.Controls.Add(this.tbBuzz);
            this.Controls.Add(this.tbFizz);
            this.Controls.Add(this.lblBuzz);
            this.Controls.Add(this.lblFizz);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.tbCap);
            this.Controls.Add(this.btnEnter);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "FizzBuzz!";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.TextBox tbCap;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.Label lblFizz;
        private System.Windows.Forms.Label lblBuzz;
        private System.Windows.Forms.TextBox tbFizz;
        private System.Windows.Forms.TextBox tbBuzz;
        private System.Windows.Forms.Label lblFizzCount;
        private System.Windows.Forms.Label lblBuzzCount;
        private System.Windows.Forms.Label lblFizzBuzzCount;
        private System.Windows.Forms.Label lblCap;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ProgressBar pbMain;
        private System.Windows.Forms.TextBox tbStart;
        private System.Windows.Forms.Label label1;
    }
}

